import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Artist {
    private String filePath;
    public String name;

    public Artist(String filePath) {
        this.filePath = filePath;
    }

    public Artist() {
    }

    public void writeArtistsList() {
        try (FileReader fileReader = new FileReader(filePath);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


/*

    public void enterArtists() {
        try (FileReader fileReader = new FileReader(filePath);
        BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            String line = "";
            while ((line = bufferedReader.readLine().toLowerCase()) != "exit") {
                System.out.println(line);
            }

        }
        catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }

        catch (Exception e) {
            System.out.println(e.getMessage());
        }


    }
}
*/
