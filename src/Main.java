import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.FileWriter;
import java.io.BufferedWriter;

public class Main {
    public static void main(String[] args) throws IOException {

        ArrayList<String> artistInput = new ArrayList<String>();


        File yourFile = new File("Artists.txt");
        String path = yourFile.getAbsolutePath();

        if (yourFile.exists()) {
            FileReader fr = new FileReader(path);
            BufferedReader br = new BufferedReader(new FileReader("Artists.txt"));
            String line;

        } else {
            System.out.println("FILE IS VOID OF CONTENT");
        }

        String input = "hi";
        String exit = "exit";
        Scanner myObj = new Scanner(System.in);  // Create a Scanner object

        while (true)
        {
            System.out.println("Please enter the artist you wish to add");
            String userInput = myObj.nextLine();
            if (userInput.equals(exit))
            {break;}
            artistInput.add(userInput);
        }

        File artListFile = new File("Artists.txt");

        if (!(artListFile.isFile())) {
            System.out.println("File not found, creating file");
            artListFile.createNewFile();
        }
       System.out.println("File found, writing artists to file.");

        String pathToFile = "Artists.txt";
        String dataToWrite ="";

        for (int i = 0; i <= artistInput.size() -1; i++){
            dataToWrite += artistInput.get(i) + "\n";
        }

        System.out.println(dataToWrite);

        try (FileWriter inputLine = new FileWriter(pathToFile);
        BufferedWriter output = new BufferedWriter(inputLine)) {
            output.write(dataToWrite);
        }
    }
}
